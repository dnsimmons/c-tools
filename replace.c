#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

char* str_replace(const char* pattern, const char* replacement, const char* subject){
	
	char *ret, *r;
	const char *p, *q;
	size_t oldlen = strlen(pattern);
	size_t count, retlen, newlen = strlen(replacement);

	if (oldlen != newlen) {
		for (count = 0, p = subject; (q = strstr(p, pattern)) != NULL; p = q + oldlen)
			count++;
		/* this is undefined if p - str > PTRDIFF_MAX */
		retlen = p - subject + strlen(p) + count * (newlen - oldlen);
	} else
		retlen = strlen(subject);

	if ((ret = malloc(retlen + 1)) == NULL)
		return NULL;

	for (r = ret, p = subject; (q = strstr(p, pattern)) != NULL; p = q + oldlen) {
		/* this is undefined if q - p > PTRDIFF_MAX */
		ptrdiff_t l = q - p;
		memcpy(r, p, l);
		r += l;
		memcpy(r, replacement, newlen);
		r += newlen;
	}
	strcpy(r, p);

	return ret;
}

void main(int argc, char** argv){

 if(argc < 4 || argc > 4){

	char* msg = ""
	"----------------------------------------------------------\n"
  	" replace \n"
	"----------------------------------------------------------\n"
	" Simple alias to PHP's str_replace() written in C. \n"
	" Replaces all occurences of a string within a string.\n"
	" By David N. Simmons <dsimmonsdesign.com> \n" 
	"----------------------------------------------------------\n"	
	" Usage: ./replace pattern replacement subject\n"
	"----------------------------------------------------------\n";

	printf("%s", msg);

  	exit(0);
 }
	
 char* result = str_replace(argv[1], argv[2], argv[3]);

 printf("%s", result);

}

