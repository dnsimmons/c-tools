#include <stdio.h>
#include <stdlib.h>
#include <string.h>

stringconcatenate(char str1[],char str2[]){

	int ln1,ln2,i;

	ln1=strlen(str1);
	ln2=strlen(str2);

	for(i=0;i<=ln2;i++){
		str1[ln1+i]=str2[i];
	}

}

void main(int argc, char** argv){

	if(argc < 3 || argc > 3){

		char* msg = ""
		"----------------------------------------------------------\n"
		" concat \n"
		"----------------------------------------------------------\n"
		" Simple string concatenation written in C. \n"
		" Concatenates two strings into a single string.\n"
		" By David N. Simmons <dsimmonsdesign.com> \n" 
		"----------------------------------------------------------\n"	
		" Usage: ./concat string string\n"
		"----------------------------------------------------------\n";

		printf("%s", msg);

		exit(0);
	}

	char* str1 = argv[1];
	char* str2 = argv[2];

	stringconcatenate(str1,str2);

	printf("%s", str1);

}
 

